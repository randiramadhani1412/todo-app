import { React, useEffect, useState } from "react";
import Model from "../models/model";
import DataLocal from "../models/dataLokal";
const Input = () => {
  const key = "dataLokal";
  const [items, setItem] = useState(DataLocal.LoadData(key));
  const [open, setOpen] = useState({ modal: false, id: "" });
  const formSubmit = (event) => {
    event.preventDefault();
    const data = new FormData(event.target);
    Model.createNewNote(
      {
        title: data.get("title"),
      },
      key
    );
    setItem(DataLocal.LoadData(key));
  };
  const changeVisibility = (id, index) => {
    setOpen({ modal: true, id: id });
  };
  const notesEdit = (event) => {
    event.preventDefault();
    const data = new FormData(event.target);
    Model.updateNoteById(
      open.id,
      {
        title: data.get("title"),
      },
      key
    );
    setOpen({ modal: false, id: "" });
    setItem(DataLocal.LoadData(key));
  };
  const dataDelete = (id) => {
    Model.deleteNoteById(id, key);

    setItem(DataLocal.LoadData(key));
  };
  useEffect(() => {
    const getItems = () => {
      const data = DataLocal.LoadData(key);
      console.log(data);
      setItem(data);
    };
    getItems();
  }, [setItem]);
  return (
    <>
      {open.modal ? (
        <>
          <div className="absolute flex w-full justify-center h-full bg-gray-800 ">
            <div className="w-1/2 flex p-3 h-24">
              <form className="flex justy-between w-full" onSubmit={notesEdit}>
                <input id="title" name="title" className="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline" type="text" required placeholder="Add Some Notes" />
                <button className="mx-3 bg-green-500 hover:bg-green-700 text-white font-bold py-2 px-4 rounded focus:outline-none focus:shadow-outline" type="submit">
                  edit
                </button>
              </form>
            </div>
          </div>
        </>
      ) : (
        <></>
      )}
      <div className="flex w-full justify-center ">
        <div className="flex w-1/2 justify-center bg-neutral-200 rounded-lg p-5">
          <form className="flex justy-between w-full" onSubmit={formSubmit}>
            <input id="title" name="title" className="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline" type="text" required placeholder="Add Some Notes" required />
            <button className="mx-3 bg-green-500 hover:bg-green-700 text-white font-bold py-2 px-4 rounded focus:outline-none focus:shadow-outline" type="submit">
              add
            </button>
          </form>
        </div>
      </div>
      <div className="flex justify-center w-full my-5 flex-col items-center ">
        <div className="flex flex-col justify-center  w-3/4 p-3 rounded-lg bg-neutral-200">
          {items.length > 0 ? (
            items.map((data, index) => {
              return (
                <>
                  <div className="flex w-full flex-row justify-between my-4">
                    <p className="flex items-center">{data.title}</p>
                    <div className="flex align-center">
                      <button
                        className="mx-3 bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded focus:outline-none focus:shadow-outline"
                        type="submit"
                        onClick={() => {
                          changeVisibility(data.id, index);
                        }}
                      >
                        edit
                      </button>
                      <button
                        className="mx-3 bg-red-500 hover:bg-red-700 text-white font-bold py-2 px-4 rounded focus:outline-none focus:shadow-outline"
                        type="submit"
                        onClick={() => {
                          dataDelete(data.id);
                        }}
                      >
                        delete
                      </button>
                    </div>
                  </div>
                </>
              );
            })
          ) : (
            <>
              <div className="flex justify-center w-full">
                <h1>Notes empty!</h1>
              </div>
            </>
          )}
        </div>
      </div>
    </>
  );
};

export default Input;
