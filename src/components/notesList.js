import React from "react";

const Noteslist = () => {
  return (
    <div className="flex justify-center w-full my-5 flex-col items-center ">
      <div className="flex flex-col justify-center  w-3/4 p-3 rounded-lg bg-neutral-200">
        <div className="flex w-full flex-row justify-between my-4">
          <p className="flex items-center">data dummy</p>
          <div className="flex align-center">
            <button className="mx-3 bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded focus:outline-none focus:shadow-outline" type="submit">
              edit
            </button>
            <button className="mx-3 bg-red-500 hover:bg-red-700 text-white font-bold py-2 px-4 rounded focus:outline-none focus:shadow-outline" type="submit">
              delete
            </button>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Noteslist;
