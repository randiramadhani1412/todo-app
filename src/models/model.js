import DataLocal from "./dataLokal";
const { v4: uuidv4 } = require("uuid");
const data = DataLocal.LoadData("dataLokal");
const Model = {
  createNewNote({ title }, key) {
    if (!title) {
      throw new Error("inputan ada yang kosong");
    }
    const newNote = {
      id: uuidv4(),
      title,
    };
    data.push(newNote);
    DataLocal.SetData(data, key);

    return true;
  },

  updateNoteById(id, newData, key) {
    const { title } = newData;

    if (!title) {
      throw new Error("inputan ada yang kosong");
    }

    const index = data.findIndex((note) => note.id === id);

    if (index === -1) {
      throw new Error("Id tidak ditemukan");
    }

    data[index] = {
      id,
      title,
    };
    DataLocal.SetData(data, key);
    return true;
  },

  deleteNoteById(id, key) {
    const index = data.findIndex((note) => note.id === id);

    if (index === -1) {
      throw new Error("Id tidak ditemukan");
    }

    data.splice(index, 1);
    DataLocal.SetData(data, key);
    return true;
  },
};

export default Model;
